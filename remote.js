'use strict';

/**
 * This is useful for creating methods that can only update some attributes so that ACLs can allow access to just those.
 * Generates a "PUT" remote method and prototype method with methodName on the model class given.
 * Generates a non-static method.
 * The method takes the attributes given as arguments and updates them on the model using "updateAttributes".
 * @param {Object} model the loopback model to add the methods to
 * @param {string} methodName the name of the method
 * @param {array<object>} attributes an array of attributes the remote function will update as defined in the
 * "remoteMethod" function of loopback's Model class.
 * @param {function} [modifyAttributes] an optional function that takes an object containing the attributes given
 * to the remote method and can modify them.
 * Can return a promise of the updated attributes object, an object with the updated attributes or undefined if it updates
 * the attributes in place.
 */
function addRemoteMethodThatUpdatesAttributes(model, methodName, attributes, modifyAttributes) {
  model.remoteMethod(
      methodName,
      {
        isStatic: false,
        description: 'Set the attributes',
        accepts: attributes,
        http: {verb: 'put', path: '/' + methodName },
        returns: {arg: 'obj', type: model, root: true},
      }
  );

  model.prototype[methodName] = function() {
    let args = arguments;
    let callback = args[args.length - 1];
    if (args.length != attributes.length + 1) {
      return callback(new Error('incorrect number of arguments given to' + methodName + ', expected ' + (attributes.length + 1) + ' but got ' + args.length));
    }

    let updateAttributeMap = {};
    attributes.forEach(function(attribute, i) {
      updateAttributeMap[attribute.arg] = args[i];
    });

    if (modifyAttributes) {
      let _this = this;
      let modifyPromise = modifyAttributes(updateAttributeMap);
      if (modifyPromise !== undefined && modifyPromise !== null) {
        if (typeof modifyPromise.then === 'function') {
          modifyPromise.then(function(updateAttributeMap) {
            _this.updateAttributes(updateAttributeMap, callback);
          });

          return;
        } else if (typeof modifyPromise === 'object') {
          updateAttributeMap = modifyPromise;
        } else {
          return callback(new Error('modifyAttributes should return a promise or an object with the updated attributes'));
        }
      }
    }

    this.updateAttributes(updateAttributeMap, callback);
  };
}

module.exports = {
  addRemoteMethodThatUpdatesAttributes: addRemoteMethodThatUpdatesAttributes,
};
